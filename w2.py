#Excercise 1
print("========== Bài tập 1 ==========")
u, v, x, y, z = 29, 12, 10, 4, 3
a=u/v
print("u/v = ",a)
#result: u/v =  2.4166666666666665
t=(u==v)
print("t=(u==v) => t = ",t)
#result: t=(u==v) =  False
c = u%x
print("u%x = ",c)
#result: u%x =  9
t=(x>=y)
print("t = (x>y) => t = ",t)
#result: t = (x>y) => t =  True
u += 5
print("u += 5 => u = ", u)
#result: u += 5 => u =  34
u %= z
print("u %= z => u = ",u)
#result: u %= z => u =  1
t =(v>x and y<z)
print("t =(v>x and y<z) => t = ", t)
#result: t =(v>x and y<z) => t =  False
h = x**z
print ("x**z = ",h)
#result: x**z =  1000
i = x//z
print("i = x//z => i = ",i)
#result: i = x//z => i =  3
####
#Excercise 2
print("========== Bài tập 2 ==========")
print("Đường tròn có bán kính r = 5 ")
r=5
per=r*2*3.14
are=r**2*3.14
print("Chu vi = ", per)
print("Diện tích = ",are)
#Excercise 3
print("========== Bài tập 3 ==========")
s = "Hi John, welcome to python programming for beginner!"
print(s)
chk = "python" in s
print("'python' in s: ", chk)
s1 = s[3:7:1]
print(s1)
sub="o"
print("Số lần xuất hiện của ký tự 'o' trong chuỗi s: ", s.count(sub))
spl = s.split()
#print("spl = ", spl)
print("Số từ trong chuỗi s: ", len(spl))
#Excercise 4
print("========== Bài tập 4 ==========")
s = "Twinkle, twinkle, little star, \n\tHow I wonder what you are! \nUp above the world so high, \n\tLike a diamond in the sky. \nTwinkle, twinkle, little star, \n\tHow I wonder what you are"
print(s)
#Excercise 5
print("========== Bài tập 5 ==========")
l = [23, 4.3, 4.2, 31, "python", 1, 5.3, 9, 1.7]
l.remove("python")
print(l)
l.sort()
print("Theo thứ tự tăng dần: ", l)
l.reverse()
print("Theo thứ tự giảm dần: ", l)