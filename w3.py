print("==================== Excercise 1: Sets ====================")
#creat set
A = {1,2,3,4,5,7}
B={2,4,5,9,12,24}
C={2,4,8}
#Iterate over all elements of set C and add each element to A and B
for i in C:
    A.add(i)
    B.add(i)
#Print out A and B after adding elements
print("A and B after adding elements")
print("A = ",A), print("B = ", B)
#Print out the intersection of A and B
print("intersection of A and B")
print(A.intersection(B))
#Print out the union of A and B
print("union of A and B")
print(A.union(B))
#Print out elements in A but not in B
print("elements in A but not in B")
print(A.difference(B))
#Print out the length of A and B
print("length of set A: ",len(A))
print("length of set B: ",len(B))
#Print out the maximum value of A union B
print("maximum value of A union B: ", max(A.union(B)))
#Print out the minimun value of A union B
print("minimum value of A union B: ", min(A.union(B)))
print("==================== Excercise 2: Tuples ====================")
#Create a tuple t of 4 elements, that are: 1, 'python', [2, 3], (4, 5)
t = (1, 'python', [2, 3], (4, 5))
#Unpack t into 6 variables, that are: 1, 'python', 2, 3, 4, 5
a,b,[c1,c2],[d1,d2] = (1, 'python', [2, 3], (4, 5))
print(a,b,c1,c2,d1,d2)
#Print out the last element of t
from operator import itemgetter
#itemgetter(1)(t)
print(itemgetter(1)(t))
#Add to t a list [2, 3]
t=t + ([2, 3],)
print(t)
#Check whether list [2, 3] is duplicated in t
def finddup(t):
    if t.count([2,3])>1:
        print("[2,3] lặp lại trong t")
    else:
        print("[2,3] KHÔNG lặp lại trong t")
print(finddup(t))
#Remove list [2, 3] from t
def Remove(t):
    final_tuple = ()
    for num in t:
        if num not in final_tuple:
            final_tuple= final_tuple+(num,)
    return final_tuple
print(Remove(t))
#Convert tuple t into a list
t1= list(t)
print("Type t1: ",type(t1))
print("==================== Excercise 3: Dictionaries ====================")
#Write a Python script to concatenate following dictionaries to create a new one
dic1={1:10, 2:20}
dic2={3:30, 4:40}
dic3={5:50, 6:60}
dic4 ={}
for dic in (dic1,dic2,dic3): dic4.update(dic)
print(dic4)
#
dic5 = dict()
for x in range(1,16):
    dic5[x] = x**2
print(dic5)
#3.2. Python script to sort ascending a dictionary by value
import operator
dic6 = {'a':4,'b':1,'c':3,'d':2}
print('Thư viện gốc: ',dic6)
sx_dic = sorted(dic6.items(), key = operator.itemgetter(1))
dic7 = dict(sx_dic)
print(type(dic7))
print("Thư viện theo giá trị tăng dần của value: ",dic7)
print(dic7.keys())
#3.3
string = "Python is an easy language to learn"
string_new = list(string)
def dem_chu(string_new):
    my_string = string_new
    my_dict={}
    for item in my_string:
        #if item in my_dict:
            #my_dict[item] +=1
        #else:
           # my_dict[item]=1
        my_dict[item]=my_string.count(item)
    print(my_dict)
print(dem_chu(string_new))
